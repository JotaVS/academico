package br.com.aula.Academico.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "disciplina")

public class Disciplina {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "nome",unique = true,nullable = false,length = 100)
    @NotNull(message = "Nome não pode ser Nulo.")
    @NotBlank(message = "Nome não pode ser Vazio.")
    private String nome;

    @Min(value = 40,message = "Carga horária abaixo da permitida.")
    private int cargaHoraria;

    @Length(max = 100,message = "Conteúdo maior que o permitido." )
    private String conteudo;

//    @NotNull(message = "Professor é obrigatório.")
//    private Pessoa professor;

    @NotNull(message = "Data não pode ser Nulo.")
    @PastOrPresent(message = "Data dever ser menor que a data atual.")
    private LocalDate dataCriacao;
}
