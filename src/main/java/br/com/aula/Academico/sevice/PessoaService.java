package br.com.aula.Academico.sevice;


import br.com.aula.Academico.model.Pessoa;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;

@Service
public class PessoaService {

    static ArrayList<Pessoa> lista = new ArrayList<>();
    static {
        lista.add(new Pessoa(1, "zezin","ze@ze","111",20,0,false,1000 ));
        lista.add(new Pessoa(1, "pedrin","pe@ze","222",20,0,false,1000 ));
        lista.add(new Pessoa(1, "gustin","gu@ze","333",20,0,false,1000 ));

    }

    public double pessoaValida(Pessoa pessoa){
        if (pessoa.getNome().isBlank()){
            return 0;
        }
        if (pessoa.getCpf().length()!= 11){
            return 0;
        }
        return pessoa.salarioLiquido();

    }

    public double reajuste(int id, double newSalario) {
        Pessoa p = procuraPessoa(id);
        if (p == null){return 0;}
        p.setSalario(newSalario);
        return p.salarioLiquido();
    }

 private  Pessoa procuraPessoa(int id){
        for (Pessoa p: lista){
            if (p.getId() == id){
                return p;
            }
        }
     return null;
 }
}
