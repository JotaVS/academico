package br.com.aula.Academico.resources;

import br.com.aula.Academico.model.Pessoa;
import br.com.aula.Academico.sevice.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pessoa")
public class PessoaResource {

    @Autowired
    PessoaService pService;
    @PostMapping
    public double calculaSalario(@RequestBody Pessoa pess){

        double salario = pService.pessoaValida(pess);

        return salario;
    }

    @PatchMapping("/{id}/reajuste")
    public double editPessoa(@PathVariable(required = true) int id,
                             @RequestParam(name = "novosalario",required = true) double newSalario){
    return pService.reajuste(id,newSalario);
    }
}
