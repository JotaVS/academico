package br.com.aula.Academico.resources;

import br.com.aula.Academico.model.Disciplina;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@RestController
@RequestMapping("/disciplina")
public class DisciplinaResource {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Disciplina saveDisciplina(@RequestBody @Valid Disciplina disciplina){
        System.out.println(disciplina.getNome());
        disciplina.setDataCriacao(LocalDate.now());

        return disciplina;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public  String handleException(Exception e){
        String err = e.getMessage();
        return err.substring(
                err.lastIndexOf("default message")+17,
                err.length()-3);

    }
}
