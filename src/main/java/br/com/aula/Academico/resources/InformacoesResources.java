package br.com.aula.Academico.resources;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class InformacoesResources {

    @GetMapping
    public String home(){
        return  "Serviço On";

    }
}
